package ru.gradient.custody.ecgdemo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Device {

    interface DataListener {
        void onData(byte[] data);
    }

    interface StateListener {
        void onStateChanged(State state);
    }

    public enum State {
        IDLE, SCANNING, CONNECTING, CONNECTED
    }

    private static final String TAG = "Device";

    private static final UUID UUID_DATA_SERVICE = UUID.fromString("0bd51666-e7cb-469b-8e4d-2742f1ba77cc");
    private static final UUID UUID_DATA_CHARACTERISTIC = UUID.fromString("e7add780-b042-4876-aae1-112855353cc1");

    private Context mContext;

    private String mDeviceName;

    private BluetoothLeScanner mScanner;
    private BluetoothGatt mGatt;
    private BluetoothDevice mDevice;
    private BluetoothGattService mService;
    private BluetoothGattCharacteristic mCharacteristic;

    private Semaphore mWriteComplete;

    private boolean mScanning = false;

    private DataListener mDataListener;
    private StateListener mStateListener;

    private State mState = State.IDLE;

    private boolean mHighPriorityConnection = true;
    private boolean mNotifications = true;

    private final ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            if (result.getDevice().getName() == null) {
                return;
            }
            if (mDeviceName == null) {
                return;
            }
            if (result.getDevice().getName().equals(mDeviceName)) {
                Log.d(TAG, "Device " + mDeviceName + " found, attempting to connect.");
                stopScan();
                mDevice = result.getDevice();
                mGatt = mDevice.connectGatt(mContext, false, mGattCallback);
                updateState(State.CONNECTING);
            }
        }
    };

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                if (mHighPriorityConnection) {
                    Log.d(TAG, "Switching connection to high priority.");
                    mGatt.requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_HIGH);
                } else {
                    Log.d(TAG, "Switching connection to default priority.");
                    mGatt.requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_BALANCED);
                }
                mGatt.discoverServices();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d(TAG, "GATT service disconnected.");
                pause();
                resume();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                mService = gatt.getService(UUID_DATA_SERVICE);
                mCharacteristic = mService.getCharacteristic(UUID_DATA_CHARACTERISTIC);
                mGatt.setCharacteristicNotification(mCharacteristic, true);
                UUID uuid = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
                BluetoothGattDescriptor descriptor = mCharacteristic.getDescriptor(uuid);
                if (mNotifications) {
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                } else {
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
                }
                mGatt.writeDescriptor(descriptor);
                updateState(State.CONNECTED);
                Log.d(TAG, "Subscribed to characteristic updates.");
            }
        }

        @Override
        public void onCharacteristicChanged(
                BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            if (mDataListener != null) {
                mDataListener.onData(characteristic.getValue());
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                                          BluetoothGattCharacteristic characteristic, int status) {
            // TODO: To handle things properly, must set up a write queue.
            mWriteComplete.release();
        }
    };

    public Device(Context context) {
        mContext = context;
        mWriteComplete = new Semaphore(1);
        mWriteComplete.tryAcquire(1);
        final BluetoothManager manager =
                (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter adapter = manager.getAdapter();
        mScanner = adapter.getBluetoothLeScanner();
    }

    public void setDataListener(DataListener listener) {
        mDataListener = listener;
    }

    public void setStateListener(StateListener listener) {
        mStateListener = listener;
    }

    public void setHighPriorityConnection(boolean enabled) {
        mHighPriorityConnection = enabled;
        Log.d(TAG, "Connection priority: " + (mHighPriorityConnection ? "high." : "default."));
    }

    public void setNotifications(boolean enabled) {
        mNotifications = enabled;
        Log.d(TAG, "Receiving data over " + (mNotifications ? "notifications." : "indications."));
    }

    public String getDeviceName() {
        return mDeviceName;
    }

    public void pause() {
        stopScan();
        if (mGatt != null) {
            mGatt.close();
            mGatt = null;
        }
        updateState(State.IDLE);
    }

    public void resume() {
        SharedPreferences preferences = mContext.getSharedPreferences("main", 0);
        mDeviceName = preferences.getString("deviceName", "");
        if (!mDeviceName.equals("")) {
            startScan();
            updateState(State.SCANNING);
        }
    }

    public boolean send(byte[] data) {
        if (mState == State.CONNECTED) {
            int length = data.length;
            int offset = 0;
            mWriteComplete.tryAcquire(1);
            while (length > 0) {
                int size = Math.min(length, 20);
                if (!mCharacteristic.setValue(Arrays.copyOfRange(data, offset, offset + size))) {
                    return false;
                }
                if (!mGatt.writeCharacteristic(mCharacteristic)) {
                    return false;
                }
                try {
                    mWriteComplete.tryAcquire(100, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {}
                offset += size;
                length -= size;
            }
            return true;
        } else {
            return false;
        }
    }

    private void startScan() {
        mScanning = true;
        ScanSettings settings =
                new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
        mScanner.startScan(new ArrayList<ScanFilter>(), settings, mScanCallback);
    }

    private void stopScan() {
        mScanning = false;
        mScanner.stopScan(mScanCallback);
    }

    private void updateState(State state) {
        mState = state;
        if (mStateListener != null) {
            mStateListener.onStateChanged(mState);
        }
    }
}
