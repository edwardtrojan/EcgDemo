package ru.gradient.custody.ecgdemo;

import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class ChooseDeviceActivity extends ListActivity {

    private static final String TAG = "ChooseDeviceActivity";

    private ArrayAdapter<String> mListViewAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mScanner;
    private Handler mHandler;
    private boolean mScanning = false;

    private static final int BT_SCAN_PERIOD = 10000;

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            mListViewAdapter.add(result.getDevice().getName());
            mListViewAdapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mHandler = new Handler();

        mListViewAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, android.R.id.text1,
                new ArrayList<String>());
        setListAdapter(mListViewAdapter);

        final BluetoothManager manager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = manager.getAdapter();
        mScanner = mBluetoothAdapter.getBluetoothLeScanner();

        startScan();
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        SharedPreferences preferences = getSharedPreferences("main", 0);
        SharedPreferences.Editor editor = preferences.edit();
        String deviceName = mListViewAdapter.getItem(position);
        editor.putString("deviceName", deviceName);
        editor.commit();
        Log.d(TAG, "User selected device: " + deviceName);
        finish();
    }

    @Override
    protected void onDestroy() {
        stopScan();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_choose_device, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void startScan() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScan();
            }
        }, BT_SCAN_PERIOD);
        mScanning = true;
        ScanSettings settings =
                new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
        mScanner.startScan(new ArrayList<ScanFilter>(), settings, mScanCallback);
    }

    private void stopScan() {
        mScanning = false;
        mScanner.stopScan(mScanCallback);
    }
}
