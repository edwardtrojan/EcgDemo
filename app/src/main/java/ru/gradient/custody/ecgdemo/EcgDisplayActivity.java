package ru.gradient.custody.ecgdemo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ru.cardiomarker.p1.rpc.activity.Type;
import ru.cardiomarker.p1.rpc.ecg.Lead;

import static android.text.format.DateUtils.formatElapsedTime;

public class EcgDisplayActivity extends Activity implements Device.StateListener,
        Statistics.Listener {

    private static final String TAG = "EcgDisplayActivity";

    private static final int INITIAL_RECORD_OFFSET = 2500;

    private Device mDevice;
    private MessageDecoder mDecoder;
    private Server mServer;
    private Statistics mStatistics;
    private Rpc mRpc;

    private LineGraphSeries<DataPoint> mEcgSeries;
    private double mEcgTimeValue = 0;

    private LineGraphSeries<DataPoint> mMeanActivitySeries;
    private LineGraphSeries<DataPoint> mRmsActivitySeries;
    private LineGraphSeries<DataPoint> mPeakActivitySeries;

    private TextView mDeviceNameView;
    private TextView mConnectionStateView;
    private TextView mMeanPacketLatency;
    private TextView mMeanPacketSize;
    private TextView mDataRate;
    private TextView mLogWindow;

    private int mFreshestRecordTimestamp = 0;
    private ru.cardiomarker.p1.rpc.ecg.Lead mFreshestRecordLead;
    private int mLastFetchedOffset = INITIAL_RECORD_OFFSET;

    private int mDownsampleCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecg_display);
        mDevice = new Device(this);
        mDecoder = new MessageDecoder();
        mStatistics = new Statistics();
        mServer = new Server();
        mDevice.setDataListener(mDecoder);
        mDevice.setStateListener(this);
        mDecoder.setStatistics(mStatistics);
        mDecoder.setMessageListener(mServer);
        mServer.setDevice(mDevice);
        mStatistics.setListener(this);
        mRpc = new Rpc(mServer);

        mDeviceNameView = (TextView) findViewById(R.id.device_name);
        mConnectionStateView = (TextView) findViewById(R.id.connection_state);
        mMeanPacketLatency = (TextView) findViewById(R.id.mean_packet_latency);
        mMeanPacketSize = (TextView) findViewById(R.id.mean_packet_size);
        mDataRate = (TextView) findViewById(R.id.data_rate);

        mLogWindow = (TextView) findViewById(R.id.log_window);
        mLogWindow.setMovementMethod(new ScrollingMovementMethod());

        GraphView plot = (GraphView) findViewById(R.id.ecg_plot);
        mEcgSeries = new LineGraphSeries<DataPoint>();
        mEcgSeries.setThickness(2);
        plot.addSeries(mEcgSeries);
        plot.getViewport().setXAxisBoundsManual(true);
        plot.getViewport().setYAxisBoundsManual(true);
        plot.getViewport().setMinX(0);
        plot.getViewport().setMaxX(750);
        plot.getViewport().setMinY(-5.2);
        plot.getViewport().setMaxY(5.2);
        plot.getGridLabelRenderer().setNumVerticalLabels(9);

        GraphView activity_plot = (GraphView) findViewById(R.id.activity_plot);

        int now = (int)(System.currentTimeMillis() / 1000);
        activity_plot.getViewport().setXAxisBoundsManual(true);
        //activity_plot.getViewport().setYAxisBoundsManual(true);
        activity_plot.getViewport().setMinX(-3600);
        activity_plot.getViewport().setMaxX(0);
        //activity_plot.getViewport().setMinY(0);
        //activity_plot.getViewport().setMaxY(9.81);

        /*mMeanActivitySeries = new LineGraphSeries<DataPoint>(new DataPoint[] { new DataPoint(now, 0) });
        activity_plot.addSeries(mMeanActivitySeries);
        mMeanActivitySeries.setTitle("Mean");
        mMeanActivitySeries.setColor(Color.BLUE);
        mMeanActivitySeries.setThickness(2);*/

        mRmsActivitySeries = new LineGraphSeries<DataPoint>(new DataPoint[] { new DataPoint(0, 0) });
        activity_plot.addSeries(mRmsActivitySeries);
        mRmsActivitySeries.setTitle("RMS");
        mRmsActivitySeries.setColor(Color.BLUE);
        mRmsActivitySeries.setThickness(2);

        /*mPeakActivitySeries = new LineGraphSeries<DataPoint>(new DataPoint[] { new DataPoint(now, 0) });
        activity_plot.addSeries(mPeakActivitySeries);
        mPeakActivitySeries.setTitle("Peak");
        mPeakActivitySeries.setColor(Color.RED);
        mPeakActivitySeries.setThickness(2);*/

        activity_plot.getLegendRenderer().setVisible(true);
        activity_plot.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.BOTTOM);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDevice.pause();
        mStatistics.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateSettings();
        mDevice.resume();
        mStatistics.resume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ecg_display, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_choose_device:
                Intent choose_device_intent = new Intent(this, ChooseDeviceActivity.class);
                startActivity(choose_device_intent);
                return true;
            case R.id.action_settings:
                Intent settings_intent = new Intent(this, SettingsActivity.class);
                startActivity(settings_intent);
                return true;
            case R.id.action_device_info:
                try {
                    mLogWindow.append("Sending info request.\n");
                    ru.cardiomarker.p1.rpc.info.get.Response info = mRpc.getInfo();
                    mLogWindow.append(" > Device ID:         " + Util.bytesToHex(info.getId().toByteArray()) + "\n");
                    mLogWindow.append(" > Firmware version:  " + info.getFirmwareVersion() + "\n");
                    mLogWindow.append(" > Hardware revision: " + info.getHardwareRevision() + "\n");
                    if (info.hasRadioModuleInfo()) {
                        mLogWindow.append(" > Radio module info: " + info.getRadioModuleInfo() + "\n");
                    }
                    mLogWindow.append(" > System time: " + new java.util.Date((long)info.getSystemTime().getSeconds() * 1000) + "\n");
                    if (info.hasEcgLeadInfo()) {
                        mLogWindow.append(" > Supported leads:");
                        for (ru.cardiomarker.p1.rpc.ecg.Lead supported_lead : info.getEcgLeadInfo().getSupportedLeadsList()) {
                            mLogWindow.append(" " + supported_lead.toString());
                        }
                        mLogWindow.append("\n");
                        mLogWindow.append(" > Selected leads:");
                        for (ru.cardiomarker.p1.rpc.ecg.Lead selected_lead : info.getEcgLeadInfo().getSelectedLeadsList()) {
                            mLogWindow.append(" " + selected_lead.toString());
                        }
                        mLogWindow.append("\n");
                        mLogWindow.append(" > Attached leads:");
                        for (ru.cardiomarker.p1.rpc.ecg.Lead attached_lead : info.getEcgLeadInfo().getAttachedLeadsList()) {
                            mLogWindow.append(" " + attached_lead.toString());
                        }
                        mLogWindow.append("\n");
                    }
                    if (info.hasStorageSummary()) {
                        mLogWindow.append(" > Stored ECG records: " + info.getStorageSummary().getEcgRecordCount() + "\n");
                        mLogWindow.append(" > Stored button events: " + info.getStorageSummary().getButtonPressCount() + "\n");
                    }
                    if (info.hasBatteryCharge()) {
                        mLogWindow.append(" > Battery charge: " + info.getBatteryCharge() + "%\n");
                    }
                } catch (Server.RequestTimeoutException e) {
                    mLogWindow.append("Request timed out: " + e + "\n");
                } catch (Server.RequestErrorException e) {
                    mLogWindow.append(e.toString() + "\n");
                }
                return true;
             case R.id.action_get_ecg_list:
                try {
                    mLogWindow.append("Querying ECG record list.\n");
                    ru.cardiomarker.p1.rpc.info.get.Response info = mRpc.getInfo();
                    int record_count = info.getStorageSummary().getEcgRecordCount();
                    int offset = record_count > 3 ? record_count - 3 : 0;
                    ru.cardiomarker.p1.rpc.ecg.list.Response ecgList = mRpc.getEcgList(offset);
                    for (ru.cardiomarker.p1.rpc.ecg.list.Response.Record record : ecgList.getRecordsList()) {
                        mLogWindow.append(" > " + new java.util.Date((long)record.getTimestamp().getSeconds() * 1000)
                                + "\t\t" + formatElapsedTime((record.getSamples() / 500))
                                + " (" + record.getSamples() + " samples)"
                                + "\t\t" + record.getLead().toString() + "\n");
                        mFreshestRecordTimestamp = record.getTimestamp().getSeconds();
                        mFreshestRecordLead = record.getLead();
                        mLastFetchedOffset = record.getSamples();
                    }
                } catch (Server.RequestErrorException e) {
                    mLogWindow.append(e.toString() + "\n");
                } catch (Server.RequestTimeoutException e) {
                    mLogWindow.append("Request timed out: " + e + "\n");
                }
                try {
                    mLogWindow.append("Querying ECG record list.\n");
                    int lCurrentSeconds = (int)(System.currentTimeMillis()/1000);
                    int offset = 0;
                    ru.cardiomarker.p1.rpc.info.get.Response info = mRpc.getInfo();
                    if(info != null && info.getStorageSummary().getEcgRecordCount() > 3) {
                        offset = info.getStorageSummary().getEcgRecordCount() - 3;
                    }
                    ru.cardiomarker.p1.rpc.ecg.list.Response ecgList = mRpc.getEcgList(offset);
                    for (ru.cardiomarker.p1.rpc.ecg.list.Response.Record record : ecgList.getRecordsList()) {
                        mLogWindow.append(" > " + new java.util.Date((long)record.getTimestamp().getSeconds() * 1000)
                                + "\t\t" + formatElapsedTime((record.getSamples() / 500))
                                + "\t\t" + record.getLead().toString() + "\n");
                        mFreshestRecordTimestamp = record.getTimestamp().getSeconds();
                        mFreshestRecordLead = record.getLead();
                    }
                    mLogWindow.append("Querying ECG record list - auto mode for 40 minutes before current time.\n");
                    lCurrentSeconds = (int)(System.currentTimeMillis()/1000);
                    ecgList = mRpc.getEcgList(lCurrentSeconds-40*60,lCurrentSeconds);
                    for (ru.cardiomarker.p1.rpc.ecg.list.Response.Record record : ecgList.getRecordsList()) {
                        mLogWindow.append(" > " + new java.util.Date((long)record.getTimestamp().getSeconds() * 1000)
                                + "\t\t" + formatElapsedTime((record.getSamples() / 500))
                                + "\t\t" + record.getLead().toString() + "\n");
                        mFreshestRecordTimestamp = record.getTimestamp().getSeconds();
                        mFreshestRecordLead = record.getLead();
                    }
                    mLastFetchedOffset = 0;
                } catch (Server.RequestErrorException e) {
                    mLogWindow.append(e.toString() + "\n");
                } catch (Server.RequestTimeoutException e) {
                    mLogWindow.append("Request timed out: " + e + "\n");
                }
                return true;
            case R.id.action_test_stream_speed:
                mLogWindow.append("Testing ECG stream speed.\n");
                try {
                    int nSamples = 0;
                    int nBytes = 0;
                    long start = System.currentTimeMillis();
                    ru.cardiomarker.p1.rpc.ecg.peek.Response ecgData = mRpc.getEcgStreamData();
                    nSamples += ecgData.getDiffsCount() + 1;
                    nBytes += ecgData.toByteArray().length;
                    long end = System.currentTimeMillis();
                    mLogWindow.append("Got " + nSamples + " samples (" + nBytes + " bytes) in " + (end - start) + " ms\n");
                    int sample = ecgData.getFirst();
                    onSample(sample, true);
                    for (int diff : ecgData.getDiffsList()) {
                        sample += diff;
                        onSample(sample, true);
                    }
                } catch (Server.RequestErrorException e) {
                    mLogWindow.append(e.toString() + "\n");
                } catch (Server.RequestTimeoutException e) {
                    mLogWindow.append("Request timed out: " + e + "\n");
                }
                return true;
            case R.id.action_read_ecg_record:
                mLogWindow.append(
                        "Reading ECG record (" +
                        new java.util.Date((long)mFreshestRecordTimestamp * 1000) +
                        ", " +
                        mFreshestRecordLead.toString() +
                        "), offset " +
                        mLastFetchedOffset +
                        ".\n");
                try {
                    long start = System.currentTimeMillis();
                    ru.cardiomarker.p1.rpc.ecg.get.Response ecgData = mRpc.getEcgRecord(mFreshestRecordTimestamp, mFreshestRecordLead, mLastFetchedOffset);
                    long end = System.currentTimeMillis();
                    int sample = ecgData.getFirst();
                    onSample(sample, false);
                    for (int diff : ecgData.getDiffsList()) {
                        sample += diff;
                        onSample(sample, false);
                    }
                    mLastFetchedOffset += ecgData.getDiffsCount() + 1;
                } catch (Server.RequestErrorException e) {
                    mLogWindow.append(e.toString() + "\n");
                } catch (Server.RequestTimeoutException e) {
                    mLogWindow.append("Request timed out: " + e + "\n");
                }
                    mLastFetchedOffset = 0;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            recording();
                         }
                    }).start();

                return true;
            case R.id.action_set_time:
                try {
                    mRpc.setTime(System.currentTimeMillis() / 1000);
                } catch (Server.RequestErrorException e) {
                    mLogWindow.append(e.toString() + "\n");
                } catch (Server.RequestTimeoutException e) {
                    mLogWindow.append("Request timed out: " + e + "\n");
                }
                return true;
            case R.id.action_get_button_events:
                try {
                    mLogWindow.append("Querying button events.\n");
                    ru.cardiomarker.p1.rpc.button_events.get.Response button_events = mRpc.getButtonEvents();
                    for (ru.cardiomarker.p1.rpc.Timestamp timestamp : button_events.getButtonEventsList()) {
                        mLogWindow.append(" > Button event: " + new java.util.Date((long)timestamp.getSeconds() * 1000) + "\n");
                    }
                } catch (Server.RequestErrorException e) {
                    mLogWindow.append(e.toString() + "\n");
                } catch (Server.RequestTimeoutException e) {
                    mLogWindow.append("Request timed out: " + e + "\n");
                }
                return true;
            case R.id.action_read_activity_data:
                try {
                    mLogWindow.append("Fetching activity data.\n");
                    /*mMeanActivitySeries.resetData(new DataPoint[] {});
                    int nsamples = 0;
                    int from = (int)(System.currentTimeMillis() / 1000) - 3600;
                    do {
                        ru.cardiomarker.p1.rpc.activity.get.Response activity = mRpc.getActivityData(Type.MEAN, from);
                        mLogWindow.append("Retrieved " + activity.getSamplesCount() + " sample(s).\n");
                        for (ru.cardiomarker.p1.rpc.activity.get.Response.Sample sample : activity.getSamplesList()) {
                            mMeanActivitySeries.appendData(new DataPoint(sample.getTimestamp().getSeconds(), sample.getActivity()), false, 100);
                            from = sample.getTimestamp().getSeconds() + 1;
                        }
                        nsamples = activity.getSamplesCount();
                    } while (nsamples > 0);*/
                    // readActivityData(Type.MEAN, mMeanActivitySeries);
                    readActivityData(Type.RMS, mRmsActivitySeries);
                    // readActivityData(Type.PEAK, mPeakActivitySeries);
                } catch (Server.RequestErrorException e) {
                    mLogWindow.append(e.toString() + "\n");
                } catch (Server.RequestTimeoutException e) {
                    mLogWindow.append("Request timed out: " + e + "\n");
                }
                return true;
            case R.id.action_switch_active_lead:
                mLogWindow.append("Switching active lead.\n");
                try {
                    ru.cardiomarker.p1.rpc.settings.get.Response settings = mRpc.getSettings();
                    if (settings.getSelectedLeadsCount() != 1) {
                        mLogWindow.append("Expected one selected lead in settings, got " + settings.getSelectedLeadsCount() + "\n");
                        return true;
                    }
                    ru.cardiomarker.p1.rpc.ecg.Lead old_lead = settings.getSelectedLeads(0);
                    ru.cardiomarker.p1.rpc.ecg.Lead new_lead = old_lead == Lead.II ? Lead.V5 : Lead.II;
                    mLogWindow.append(">> Currently selected lead: " + old_lead.toString() + ", switching to " + new_lead + "...\n");
                    mRpc.selectLead(new_lead);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ru.cardiomarker.p1.rpc.settings.get.Response new_settings = mRpc.getSettings();
                                if (new_settings.getSelectedLeadsCount() != 1) {
                                    mLogWindow.append("Expected one selected lead in settings, got " + new_settings.getSelectedLeadsCount() + "\n");
                                }
                                mLogWindow.append(">> Now selected: " + new_settings.getSelectedLeads(0).toString());
                            } catch (Server.RequestErrorException e) {
                                mLogWindow.append(e.toString() + "\n");
                            } catch (Server.RequestTimeoutException e) {
                                mLogWindow.append("Request timed out: " + e + "\n");
                            }
                        }
                    }, 1000);
                } catch (Server.RequestErrorException e) {
                    mLogWindow.append(e.toString() + "\n");
                } catch (Server.RequestTimeoutException e) {
                    mLogWindow.append("Request timed out: " + e + "\n");
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void recording() {
        String lDirPath = Environment.getExternalStorageDirectory().getPath() + "/CardioMarker";
        WavRecorder lRecorder =
                new WavRecorder(
                        lDirPath,
                        500,//settingsProvider.getSampleRate(),
                        500,//settingsProvider.getRecordSampleRate(),
                        mFreshestRecordLead,
                        this);

        lRecorder.setAnthropometry(
                25,
                true,
                100,
                100,
                0);

        lRecorder.startRecording(mFreshestRecordTimestamp);

        for(int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();
            ru.cardiomarker.p1.rpc.ecg.get.Response ecgData = null;
            try {
                ecgData = mRpc.getEcgRecord(mFreshestRecordTimestamp, mFreshestRecordLead, mLastFetchedOffset);
            } catch (Server.RequestErrorException e) {
                e.printStackTrace();
            } catch (Server.RequestTimeoutException e) {
                e.printStackTrace();
            }
            long end = System.currentTimeMillis();
            if(ecgData != null && ecgData.hasFirst()) {
                List<Float> lList = new ArrayList<>();
                int sample = ecgData.getFirst();
                lList.add((float) sample / 100000f);
                //onSample(sample, true);
                for (int diff : ecgData.getDiffsList()) {
                    sample += diff;
                    lList.add((float) sample / 100000f);
                    //onSample(sample, true);
                }
                Log.d("read", "num=" + i + " time=" + (end - start) + " size=" + lList.size());
                try {
                    TimeUnit.MILLISECONDS.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                lRecorder.pushFloatValues(lList);

                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mLastFetchedOffset += ecgData.getDiffsCount() + 1;
            }
        }
        lRecorder.stopRecording(false);
    }

    //@Override
    /*public void onSample(final int sample, boolean downsample) {
        mDownsampleCounter = (mDownsampleCounter + 1) % 5;
        if (downsample && mDownsampleCounter != 0) {
            return;
        }
        final double s = (double)sample / 100000f;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEcgSeries.appendData(new DataPoint(mEcgTimeValue, s), true, 250);
                mEcgTimeValue += 1;
            }
        });
    }*/
    private void readActivityData(ru.cardiomarker.p1.rpc.activity.Type type, LineGraphSeries<DataPoint> series) throws Server.RequestTimeoutException, Server.RequestErrorException {
        series.resetData(new DataPoint[] {});
        int nsamples = 0;
        int now = (int)(System.currentTimeMillis() / 1000);
        int from = now - 3600;
        do {
            ru.cardiomarker.p1.rpc.activity.get.Response activity = mRpc.getActivityData(type, from);
            mLogWindow.append("Retrieved " + activity.getSamplesCount() + " sample(s).\n");
            for (ru.cardiomarker.p1.rpc.activity.get.Response.Sample sample : activity.getSamplesList()) {
                try {
                    series.appendData(new DataPoint(sample.getTimestamp().getSeconds() - now, sample.getActivity()), false, 360);
                } catch (IllegalArgumentException e) {
                    // Do nuthin'.
                }
                from = sample.getTimestamp().getSeconds() + 1;
            }
            nsamples = activity.getSamplesCount();
        } while (nsamples > 0);
    }

    public void onSample(final int sample, final boolean upsample) {
        final double s = (double)sample / 100000f;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEcgSeries.appendData(new DataPoint(mEcgTimeValue, s), true, 750);
                mEcgTimeValue += 1;
                if (upsample) {
                    mEcgSeries.appendData(new DataPoint(mEcgTimeValue, s), true, 750);
                    mEcgTimeValue += 1;
                    mEcgSeries.appendData(new DataPoint(mEcgTimeValue, s), true, 750);
                    mEcgTimeValue += 1;
                    mEcgSeries.appendData(new DataPoint(mEcgTimeValue, s), true, 750);
                    mEcgTimeValue += 1;
                    mEcgSeries.appendData(new DataPoint(mEcgTimeValue, s), true, 750);
                    mEcgTimeValue += 1;
                }
            }
        });
    }

    @Override
    public void onStateChanged(final Device.State state) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDeviceNameView.setText(mDevice.getDeviceName());
                switch (state) {
                    case IDLE:
                        mConnectionStateView.setText("IDLE");
                        mLogWindow.append("### IDLE ###\n");
                        break;
                    case SCANNING:
                        mConnectionStateView.setText("SCANNING");
                        mLogWindow.append("### SCANNING ###\n");
                        break;
                    case CONNECTING:
                        mConnectionStateView.setText("CONNECTING");
                        mLogWindow.append("### CONNECTING ###\n");
                        break;
                    case CONNECTED:
                        mConnectionStateView.setText("CONNECTED");
                        mLogWindow.append("### CONNECTED ###\n");
                        break;
                }
            }
        });
    }

    private void updateSettings() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        mDevice.setHighPriorityConnection(preferences.getBoolean("prefs_key_high_connection_priority", true));
        mDevice.setNotifications(preferences.getBoolean("prefs_key_use_notifications", true));
    }

    @Override
    public void onStatisticsUpdated() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMeanPacketLatency.setText(String.format("%.2f ms/packet", mStatistics.meanPacketLatency));
                mMeanPacketSize.setText(String.format("%.2f bytes/packet", mStatistics.meanPacketSize));
                mDataRate.setText(String.format("%.2f kbytes/s", mStatistics.dataRate / 1024));
            }
        });
    }
}
