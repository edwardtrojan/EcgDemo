package ru.gradient.custody.ecgdemo;

import android.util.Log;

import com.google.protobuf.InvalidProtocolBufferException;

import ru.cardiomarker.p1.rpc.Timestamp;
import ru.cardiomarker.p1.rpc.ecg.Lead;

public class Rpc {
    private Server mServer;

    public Rpc(Server server) {
        mServer = server;
    }

    ru.cardiomarker.p1.rpc.info.get.Response getInfo() throws Server.RequestErrorException, Server.RequestTimeoutException {
        byte[] responseBody = mServer.sendRequest((byte)0, "".getBytes());
        try {
            return ru.cardiomarker.p1.rpc.info.get.Response.parseFrom(responseBody);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    ru.cardiomarker.p1.rpc.settings.get.Response getSettings() throws Server.RequestErrorException, Server.RequestTimeoutException {
        byte[] responseBody = mServer.sendRequest((byte)0x10, "".getBytes());
        try {
            return ru.cardiomarker.p1.rpc.settings.get.Response.parseFrom(responseBody);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    ru.cardiomarker.p1.rpc.ecg.list.Response getEcgList(int offset) throws Server.RequestErrorException, Server.RequestTimeoutException {
        ru.cardiomarker.p1.rpc.ecg.list.Request request =
                ru.cardiomarker.p1.rpc.ecg.list.Request.newBuilder()
                .addLeads(Lead.II).addLeads(Lead.V5)
                .clearLimit().setOffset(offset)
                .clearFrom().clearTo()
                .build();
        byte[] requestBody = request.toByteArray();
        byte[] responseBody = mServer.sendRequest((byte)0x20, requestBody);
        try {
            return ru.cardiomarker.p1.rpc.ecg.list.Response.parseFrom(responseBody);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
    public ru.cardiomarker.p1.rpc.ecg.list.Response getEcgList(int startTime, int endTime)//, int aLimit)
            throws Server.RequestErrorException, Server.RequestTimeoutException {
        ru.cardiomarker.p1.rpc.ecg.list.Request request =
                ru.cardiomarker.p1.rpc.ecg.list.Request.newBuilder()
                        .addLeads(Lead.II)
                        .clearLimit().clearOffset()
                        //.clearFrom().clearTo()//.setLimit(aLimit).clearOffset()
                        .setFrom(Timestamp.newBuilder().setSeconds(startTime).build())
                        .setTo(Timestamp.newBuilder().setSeconds(endTime).build())
                        .build();
        java.util.Date dStart = new java.util.Date((long)startTime*1000);
        java.util.Date dEnd = new java.util.Date((long)endTime*1000);
        byte[] requestBody = request.toByteArray();
                byte[] responseBody = mServer.sendRequest((byte)0x20, requestBody);
                try {
                    return ru.cardiomarker.p1.rpc.ecg.list.Response.parseFrom(responseBody);
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                    throw new RuntimeException();
                }
    }

    ru.cardiomarker.p1.rpc.ecg.peek.Response getEcgStreamData() throws Server.RequestErrorException, Server.RequestTimeoutException {
        ru.cardiomarker.p1.rpc.ecg.peek.Request request =
                ru.cardiomarker.p1.rpc.ecg.peek.Request.newBuilder()
                .setLead(Lead.II)
                .clearLimit()
                .build();
        byte[] requestBody = request.toByteArray();
        byte[] responseBody = mServer.sendRequest((byte)0x23, requestBody);
        try {
            return ru.cardiomarker.p1.rpc.ecg.peek.Response.parseFrom(responseBody);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    ru.cardiomarker.p1.rpc.ecg.get.Response getEcgRecord(int timestamp, ru.cardiomarker.p1.rpc.ecg.Lead lead, int offset) throws Server.RequestErrorException, Server.RequestTimeoutException {
        Timestamp t = ru.cardiomarker.p1.rpc.Timestamp.newBuilder()
                .clearMillis()
                .setSeconds(timestamp)
                .build();
        ru.cardiomarker.p1.rpc.ecg.get.Request request =
                ru.cardiomarker.p1.rpc.ecg.get.Request.newBuilder()
                .setTimestamp(t)
                .setLead(lead)
                .clearLimit()
                .setOffset(offset)
                .build();
        byte[] requestBody = request.toByteArray();
        byte[] responseBody = mServer.sendRequest((byte)0x21, requestBody);
        try {
            return ru.cardiomarker.p1.rpc.ecg.get.Response.parseFrom(responseBody);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    void setTime(long time) throws Server.RequestErrorException, Server.RequestTimeoutException {
        Timestamp t = ru.cardiomarker.p1.rpc.Timestamp.newBuilder()
                .clearMillis()
                .setSeconds((int)time)
                .build();
        ru.cardiomarker.p1.rpc.settings.set.Request request =
                ru.cardiomarker.p1.rpc.settings.set.Request.newBuilder()
                .setTime(t)
                .clearSelectedLeads()
                .build();
        byte[] requestBody = request.toByteArray();
        byte[] responseBody = mServer.sendRequest((byte)0x11, requestBody);
    }

    void selectLead(ru.cardiomarker.p1.rpc.ecg.Lead lead) throws Server.RequestErrorException, Server.RequestTimeoutException {
        ru.cardiomarker.p1.rpc.settings.set.Request request =
                ru.cardiomarker.p1.rpc.settings.set.Request.newBuilder()
                        .clearTime()
                        .clearSelectedLeads()
                        .addSelectedLeads(lead)
                        .build();
        byte[] requestBody = request.toByteArray();
        byte[] responseBody = mServer.sendRequest((byte)0x11, requestBody);
    }

    ru.cardiomarker.p1.rpc.button_events.get.Response getButtonEvents() throws Server.RequestErrorException, Server.RequestTimeoutException {
        ru.cardiomarker.p1.rpc.button_events.get.Request request =
                ru.cardiomarker.p1.rpc.button_events.get.Request.newBuilder()
                .clear()
                .build();
        byte[] requestBody = request.toByteArray();
        byte[] responseBody = mServer.sendRequest((byte)0x80, requestBody);
        try {
            return ru.cardiomarker.p1.rpc.button_events.get.Response.parseFrom(responseBody);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    ru.cardiomarker.p1.rpc.activity.get.Response getActivityData(ru.cardiomarker.p1.rpc.activity.Type type, int from) throws Server.RequestErrorException, Server.RequestTimeoutException {
        ru.cardiomarker.p1.rpc.activity.get.Request request =
                ru.cardiomarker.p1.rpc.activity.get.Request.newBuilder()
                .clear()
                .setFrom(ru.cardiomarker.p1.rpc.Timestamp.newBuilder().setSeconds(from).build())
                .setType(type)
                .build();
        byte[] requestBody = request.toByteArray();
        byte[] responseBody = mServer.sendRequest((byte)0x40, requestBody);
        try {
            return ru.cardiomarker.p1.rpc.activity.get.Response.parseFrom(responseBody);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

}
