package ru.gradient.custody.ecgdemo;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.Formatter;

public class MessageDecoder implements Device.DataListener {

    private static final String TAG = "MessageDecoder";

    interface MessageListener {
        void onMessage(byte[] message);
    }

    private boolean mEscaped = false;

    private MessageListener mMessageListener;

    private Statistics mStatistics;

    private ByteArrayOutputStream mCurrentMessage = new ByteArrayOutputStream();

    public MessageDecoder() {
    }

    public void setMessageListener(MessageListener listener) {
        mMessageListener = listener;
    }

    public void setStatistics(Statistics statistics) {
        mStatistics = statistics;
    }

    @Override
    public void onData(byte[] data) {
        Formatter formatter = new Formatter();
        for (byte b : data) {
            formatter.format("%02x ", b);
        }
        Log.d(TAG, "Got data, length = " + data.length + " (" + formatter.toString() + ")");
        if (mStatistics != null)
            mStatistics.registerPacket(data.length);
        for (byte b : data) {
            decode(b);
        }
    }

    private void decode(byte b) {
        switch (b) {
            case 0x7e:
                ProcessMessage();
                mCurrentMessage.reset();
                mEscaped = false;
                break;
            case 0x7d:
                mEscaped = true;
                break;
            default:
                if (mEscaped) {
                    mCurrentMessage.write(b ^ 0x20);
                    mEscaped = false;
                } else {
                    mCurrentMessage.write(b);
                }
                break;
        }
    }

    private void ProcessMessage() {
        byte[] message = mCurrentMessage.toByteArray();
        if (mMessageListener != null && message.length != 0) {
            Log.d(TAG, "Got message, length = " + message.length);
            if (!Util.ValidateMessage(message)) {
                Log.d(TAG, "Malformed message received.");
                return;
            }
            mMessageListener.onMessage(message);
        }
    }
}
