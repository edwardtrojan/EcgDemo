package ru.gradient.custody.ecgdemo;

import android.os.Handler;

public class Statistics {

    interface Listener {
        void onStatisticsUpdated();
    }

    private static final int INTERVAL = 3000;

    private int mTotalPackets = 0;
    private int mTotalLength = 0;

    private int mPackets = 0;
    private int mLength = 0;

    public double meanPacketSize = 0;
    public double meanPacketLatency = 0;
    public double dataRate = 0;

    private Handler mHandler;

    private Listener mListener;

    private Runnable mUpdateTask = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mUpdateTask, INTERVAL);
            updateStatistics();
        }
    };

    public Statistics() {
        mHandler = new Handler();
    }

    public void registerPacket(int length) {
        mTotalPackets += 1;
        mTotalLength += length;
        mPackets += 1;
        mLength += length;
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public void pause() {
        mHandler.removeCallbacks(mUpdateTask);
    }

    public void resume() {
        mUpdateTask.run();
    }

    private void updateStatistics() {
        meanPacketSize = mPackets != 0 ? (double) mLength / mPackets : -1;
        meanPacketLatency = mPackets != 0 ? (double) INTERVAL / mPackets : -1;
        dataRate = mPackets != 0 ? (double) mLength / INTERVAL * 1000 : -1;
        mPackets = 0;
        mLength = 0;
        if (mListener != null) {
            mListener.onStatisticsUpdated();
        }
    }
}
