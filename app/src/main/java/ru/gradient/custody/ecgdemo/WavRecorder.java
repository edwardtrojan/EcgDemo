package ru.gradient.custody.ecgdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.cardiomarker.p1.rpc.ecg.Lead;

/**
 * Created by Dart on 04.06.2015.
 */
public class WavRecorder {

	private static final String TAG = "Recorder";
	public static final Charset US_ASCII = Charset.forName("US-ASCII");
	public String getFileName() { return mFileName; }
	private short WAVE_FORMAT_PCM = 1;
	private short CHANNELS_MONO = 1;
	private short BLOCK_ALIGN_2_BYTES = 2;
	private short BLOCK_ALIGN_3_BYTES = 3;
	private short BITS_PER_SAMPLE_16 = 16;
    private short BITS_PER_SAMPLE_24 = 24;
private String mFileName = "";
	private RandomAccessFile mRandomAccessFile = null;

	private int mDataSize = 0;
	private int mDataChunkPosition;

	private final String mDirPath;
	private final int mOutputSampleRate;
	private final int mInputSampleRate;
	private final Context mContext;

	private long mId = 0;
	private int mCalibration = 0;

	private double mAge;
	private boolean mMale;
	private double mWeight;
	private double mHeight;
	private double mBmi;

	public boolean RecordingStarted = false;
	//EcgType mEcgType = EcgType.auto;
	Lead mLead = Lead.II;
//	private String mFirstName = "";
//	private String mLastName = "";
//	private String mPlace = "";
//	private String mEmail = "";
	private int sampleCount = 0;
	private File mFileToRecord = null;
	private int mCopyNumber = 1;//mOutputSampleRate / mInputSampleRate;
	//public final List<Short> values = new ArrayList<>();
//	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			try {
//				if(!RecordingStarted) return;
//				boolean lCanceled = intent.getBooleanExtra(BluetoothController.BROADCAST_VALUE_RECORDING_STATUS_CANCELLED,false);
//				if(lCanceled)//уже не используется
//					stopRecording(true);
//				else {
//					boolean lStop = intent.getBooleanExtra(BluetoothController.BROADCAST_VALUE_RECORDING_STATUS_STOPPED, false);
//					if (lStop)
//						stopRecording(false);
//					else {
//						ArrayList<Integer> samples = intent.getIntegerArrayListExtra(BluetoothController.BROADCAST_SAMPLE_ARG);
//						int count = samples.size();//intent.getIntExtra(EcgBluetoothService.BROADCAST_SAMPLES_COUNT_ARG, 0);
//						if(count != 251)
//							Log.d(TAG, "start record to file " + count);//логируем ошибки полученного сигнала
//						if (count > 0)
//							pushValues(samples);
//						//Log.d(TAG, "end record to file " + count);
//					}
//				}
//			} catch (Exception e) {
//				Log.e(TAG, e.getMessage());
//			}
//		}
//	};

	public WavRecorder(String dirPath, int inputSampleRate, int outputSampleRate, Lead aLead, Context context) {

		mDirPath = dirPath;
		mInputSampleRate = inputSampleRate;
		mOutputSampleRate = outputSampleRate;
		mCopyNumber = mOutputSampleRate / mInputSampleRate;
		if(mCopyNumber < 1) mCopyNumber = 1;
		mContext = context;
		//mEcgType = aType;
		mLead = aLead;
	}

//	public EcgType getEcgType() {
//		return mEcgType;
//	}
//	public void setUserParams(String firstName, String lastName, String place, String email) {
//
//		mFirstName = firstName;
//		mLastName = lastName;
//		mPlace = place;
//		mEmail = email;
//	}

	public void setAnthropometry(double age, boolean maleSex, double weight, double height, double bmi) {

		mAge = age;
		mMale = maleSex;
		mWeight = weight;
		mHeight = height;
		mBmi = bmi;
	}

	public void startRecording(long aStartTime) {

		if(RecordingStarted)
			return;

//		mContext.registerReceiver(broadcastReceiver,
//				new IntentFilter(BluetoothController.BROADCAST_ACTION_RECORD));

		mDataSize = 0;
		RecordingStarted = true;

		ByteBuffer riffChunk = ByteBuffer.allocate(12);
		riffChunk.order(ByteOrder.LITTLE_ENDIAN);
		riffChunk.put("RIFF".getBytes(US_ASCII)); // ckID
		riffChunk.putInt(0); // ckSize
		riffChunk.put("WAVE".getBytes(US_ASCII));

		ByteBuffer formatChunk = ByteBuffer.allocate(24);
		formatChunk.order(ByteOrder.LITTLE_ENDIAN);
		formatChunk.put("fmt ".getBytes(US_ASCII));
		formatChunk.putInt(16); // ckSize
		formatChunk.putShort(WAVE_FORMAT_PCM);
		formatChunk.putShort(CHANNELS_MONO);
		formatChunk.putInt(mOutputSampleRate);
		final int avSamples = mOutputSampleRate * 3;
		formatChunk.putInt(avSamples);
		formatChunk.putShort(BLOCK_ALIGN_3_BYTES);
		formatChunk.putShort(BITS_PER_SAMPLE_24);
//        formatChunk.putShort(BITS_PER_SAMPLE_16);

		byte[] ecgChunk = getEcgChunk();

		ByteBuffer dataChunk = ByteBuffer.allocate(8);
		dataChunk.order(ByteOrder.LITTLE_ENDIAN);
		dataChunk.put("data".getBytes(US_ASCII));
		dataChunk.putInt(0); // ckSize

		File dir = new File(mDirPath);
		dir.mkdirs();

		String dateTimePart = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date(aStartTime));
		String fileName = dateTimePart + "_" + mOutputSampleRate + "_hz_int24.wav";


/*		String fileName =
				mLastName + " " + mFirstName + "_" +
						dateTimePart + "_1_" +
						mPlace + "_" +
						mOutputSampleRate + "hz_int16.wav";
*/
		//String fileName  = new SimpleDateFormat("'ECG 'yyyy-MM-dd HH-mm-ss'.wav'").format(new Date());
		mFileToRecord = new File(mDirPath + "/" + fileName);

		if(mFileToRecord.exists())
			mFileToRecord.delete();

		try {
			mRandomAccessFile = new RandomAccessFile(mFileToRecord, "rw");
			mFileName = mFileToRecord.getPath();
			Log.i(TAG,"start record "+mFileName);

			mRandomAccessFile.write(riffChunk.array());
			mRandomAccessFile.write(formatChunk.array());
			mRandomAccessFile.write(ecgChunk);

			mDataChunkPosition = (int) mRandomAccessFile.getFilePointer();
			mRandomAccessFile.write(dataChunk.array());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

//	public void stopRecording() {
//		mContext.unregisterReceiver(broadcastReceiver);
//		stopRecording(false);
//	}

	public String stopRecording(boolean cancel) {

		Log.d(TAG,"stop recording ");
		String path = "";
		if(!RecordingStarted) return  path;
//		try {
//			mContext.unregisterReceiver(broadcastReceiver);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		if(mFileToRecord != null) {
			path = mFileToRecord.getPath();
		}
		Log.d(TAG,"stop recording "+path);

//		if(!RecordingStarted) {
//            return path;
//        }

		synchronized (this) {
			RecordingStarted = false;
		}

		if(mFileToRecord != null && mFileToRecord.exists()) {
			if (cancel) {
				Log.d(TAG,"delete file");
				mFileToRecord.delete();
			} else {
				try {
					mRandomAccessFile.seek(4);
					ByteBuffer tempBuffer = ByteBuffer.allocate(4);
					tempBuffer.order(ByteOrder.LITTLE_ENDIAN);
					//mDataSize -= 2;//уменьшим из-за проблем записи последних двух байт
					tempBuffer.putInt(mDataChunkPosition + 8 + mDataSize - 8);
					mRandomAccessFile.write(tempBuffer.array()); // ckSize

					mRandomAccessFile.seek(mDataChunkPosition + 4);
					tempBuffer.putInt(0, mDataSize);
					mRandomAccessFile.write(tempBuffer.array()); // ckSize
					mRandomAccessFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

//				Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//				Uri uri = Uri.fromFile(mFileToRecord);
//				intent.setData(uri);
//				mContext.sendBroadcast(intent);
			}
		}
		mFileToRecord = null;
		return path;
	}

	public synchronized void pushValue(int value) {

		if(mRandomAccessFile == null || !RecordingStarted)
			return;

		ByteBuffer tempBuffer = ByteBuffer.allocate(3 * mCopyNumber);
		tempBuffer.order(ByteOrder.LITTLE_ENDIAN);

		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		byte[] result = new byte[3];

		buffer.putInt(value);
		buffer.position(1);
		buffer.get(result, 0, 3);
		buffer.rewind();
			//Log.d("record","N"+sampleCount+" val="+value + " int="+tempValue+" first="+first+" sec="+second+" third="+third);
		sampleCount++;
		for (int i = 0; i < mCopyNumber; i++) {
			tempBuffer.put(result);
		}
		try {
			mRandomAccessFile.write(tempBuffer.array());
			mDataSize += 3*mCopyNumber;
			sampleCount+= mCopyNumber;
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}

	public synchronized void pushFloatValues(List<Float> values) {

		if(mRandomAccessFile == null || !RecordingStarted)
			return;
		int len = values.size();

		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		byte[] result = new byte[3];
		ByteBuffer tempBuffer = ByteBuffer.allocate(3 * mCopyNumber*len);
		tempBuffer.order(ByteOrder.LITTLE_ENDIAN);

		for ( float value : values ) {
			int tempValue = (int)(value*100000f) << 8;//
			buffer.putInt(tempValue);
			buffer.position(1);
			buffer.get(result, 0, 3);
			buffer.rewind();
			sampleCount++;
			for (int i = 0; i < mCopyNumber; i++) {
				tempBuffer.put(result);
			}
		}
		try {
			mRandomAccessFile.write(tempBuffer.array());
			mDataSize += 3*mCopyNumber*len;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		Log.d("record"," size="+values.size() + " bytes="+tempBuffer.array().length);

	}

	public synchronized void pushValues(List<Integer> values) {

		if(mRandomAccessFile == null || !RecordingStarted)
			return;
		int len = values.size();

		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		byte[] result = new byte[3];
		ByteBuffer tempBuffer = ByteBuffer.allocate(3 * mCopyNumber*len);
		tempBuffer.order(ByteOrder.LITTLE_ENDIAN);

		try {
			for ( int value : values ) {
                buffer.putInt(value << 8);
                buffer.position(1);
                buffer.get(result, 0, 3);
                buffer.rewind();
                sampleCount++;
                for (int i = 0; i < mCopyNumber; i++) {
                    tempBuffer.put(result);
                }
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			mRandomAccessFile.write(tempBuffer.array());
			mDataSize += 3*mCopyNumber*len;
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}

	private byte[] getEcgChunk() {

		byte[] anthChunk = getAnthropometryChunk();

		int chunkSize = 8 * 3 + 4 + 8 + 4 + 12+ anthChunk.length;

		ByteBuffer ecgChunk = ByteBuffer.allocate(chunkSize + 8);
		ecgChunk.order(ByteOrder.LITTLE_ENDIAN);
		ecgChunk.put("ecg ".getBytes(US_ASCII));
		ecgChunk.putInt(chunkSize); // ckSize

		ecgChunk.put("id  ".getBytes(US_ASCII));
		ecgChunk.putInt(8); // ckSize
		ecgChunk.putLong(mId);

		ecgChunk.put("clbr".getBytes(US_ASCII));
		ecgChunk.putInt(4); // ckSize
		ecgChunk.putInt(mCalibration);

		ecgChunk.put("type".getBytes(US_ASCII));
		ecgChunk.putInt(4); // ckSize
		ecgChunk.putInt(1);

		ecgChunk.put("lead".getBytes(US_ASCII));
		ecgChunk.putInt(4); // ckSize
		ecgChunk.putInt(mLead.getNumber());

		ecgChunk.put(anthChunk);

		return ecgChunk.array();
	}

	private byte[] getAnthropometryChunk() {

		int chunkSize = 8 * 5 + 4 + 8 * 4;

		ByteBuffer anthropometryChunk = ByteBuffer.allocate(chunkSize + 8);
		anthropometryChunk.order(ByteOrder.LITTLE_ENDIAN);
		anthropometryChunk.put("anth".getBytes(US_ASCII));
		anthropometryChunk.putInt(chunkSize); // ckSize

		anthropometryChunk.put("sex ".getBytes(US_ASCII));
		anthropometryChunk.putInt(4); // ckSize
		anthropometryChunk.put((mMale ? "male" : "fem ").getBytes(US_ASCII));

		anthropometryChunk.put("age ".getBytes(US_ASCII));
		anthropometryChunk.putInt(8); // ckSize
		anthropometryChunk.putDouble(mAge);

		anthropometryChunk.put("wght".getBytes(US_ASCII));
		anthropometryChunk.putInt(8); // ckSize
		anthropometryChunk.putDouble(mWeight);

		anthropometryChunk.put("hght".getBytes(US_ASCII));
		anthropometryChunk.putInt(8); // ckSize
		anthropometryChunk.putDouble(mHeight);

		anthropometryChunk.put("bmi ".getBytes(US_ASCII));
		anthropometryChunk.putInt(8); // ckSize
		anthropometryChunk.putDouble(mBmi);

		return anthropometryChunk.array();
	}
}
