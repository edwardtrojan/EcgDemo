package ru.gradient.custody.ecgdemo;

import android.util.Log;

import com.google.protobuf.InvalidProtocolBufferException;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import ru.cardiomarker.p1.rpc.Error;

public class Server implements MessageDecoder.MessageListener {

    public class RequestTimeoutException extends Exception {
        public RequestTimeoutException(String message) {
            super(message);
        }
    }

    public class RequestErrorException extends Exception {

        private Error error;

        public RequestErrorException(Error error) {
            super(error.getMessage());
            this.error = error;
        }

        @Override
        public String toString() {
            return "Error: " + error.getStatus().toString() + ", message: " + error.getMessage();
        }
    }

    private static final String TAG = "Server";

    private LinkedBlockingQueue<byte[]> mResponseQueue = new LinkedBlockingQueue<>();

    private short mCurrentSequenceNumber;

    private Device mDevice = null;

    public void setDevice(Device device) {
        mDevice = device;
    }

    @Override
    public void onMessage(byte[] message) {
        mResponseQueue.offer(message);
    }

    public byte[] sendRequest(byte procedureId, byte[] data) throws RequestTimeoutException,
            RequestErrorException {
        mCurrentSequenceNumber += 1;
        Log.d(TAG, "Sending message, seqno = " + mCurrentSequenceNumber);
        byte[] message = Util.CreateMessage((byte)0, mCurrentSequenceNumber, procedureId, data);
        byte[] encoded = Util.EncodeMessage(message);
        mResponseQueue.clear();
        mDevice.send(encoded);
        byte[] response = null;
        try {
            response = mResponseQueue.poll(1500, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw new RequestTimeoutException("Request execution was interrupted.");
        }
        if (response == null) {
            throw new RequestTimeoutException("Request timed out.");
        }
        ByteBuffer bb = ByteBuffer.wrap(response);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.position(1);
        short seqno = bb.getShort();
        if (seqno != mCurrentSequenceNumber) {
            Log.d(TAG, "Sequence number mismatch, expected " + mCurrentSequenceNumber + ", got " + seqno);
            throw new RequestTimeoutException("Received stray response - sequence numbers don't match.");
        } else {
            Log.d(TAG, "Got response with (correct) seqno " + seqno);
        }
        byte[] responseBody = Arrays.copyOfRange(response, 3, response.length - 2);
        if (response[0] == 0x2) {
            Error error = null;
            try {
                error = Error.parseFrom(responseBody);
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            throw new RequestErrorException(error);
        }
        return responseBody;
    }
}
