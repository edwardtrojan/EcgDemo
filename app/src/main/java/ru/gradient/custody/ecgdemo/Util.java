package ru.gradient.custody.ecgdemo;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class Util {
    public static byte[] Crc16(byte[] data) {
        int crc = 0xffff;
        int polynomial = 0x1021;
        for (byte b : data) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b >> (7 - i) & 1) == 1);
                boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) crc ^= polynomial;
            }
        }
        ByteBuffer result = ByteBuffer.allocate(2);
        result.order(ByteOrder.LITTLE_ENDIAN);
        result.putShort((short)(crc & 0xffff));
        return result.array();
    }

    public static byte[] CreateMessage(
            byte type, short sequence_number, byte procedure_id, byte[] body) {
        byte[] message = new byte[4 + body.length + 2];
        final ByteBuffer mb = ByteBuffer.wrap(message);
        mb.order(ByteOrder.LITTLE_ENDIAN);
        mb.put(type);
        mb.putShort(sequence_number);
        mb.put(procedure_id);
        mb.put(body);
        mb.put(Crc16(Arrays.copyOfRange(message, 0, 4 + body.length)));
        return message;
    }

    public static byte[] EncodeMessage(byte[] message) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        stream.write(0x7e);
        for (byte b : message) {
            if (b == 0x7e || b == 0x7d) {
                stream.write(0x7d);
                stream.write(b ^ 0x20);
            } else {
                stream.write(b);
            }
        }
        stream.write(0x7e);
        return stream.toByteArray();
    }

    public static boolean ValidateMessage(byte[] message) {
        if (message.length < 5) return false;
        if (message[0] != 0x1 && message[0] != 0x1 && message[0] != 0x2) return false;
        byte[] crc1 = Arrays.copyOfRange(message, message.length - 2, message.length);
        byte[] crc2 = Util.Crc16(Arrays.copyOfRange(message, 0, message.length - 2));
        return Arrays.equals(crc1, crc2);
    }

    final private static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xff;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0f];
        }
        return new String(hexChars);
    }
}
